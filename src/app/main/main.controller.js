export class MainController {
  constructor ($scope, $sce, $filter, getBlogDataService) {
    'ngInject';

    $scope.groupedPosts = [];
    $scope.postsPerPage = 10;
    $scope.pagedPosts = [];
    $scope.currentPage = 0;

    $scope.groupToPages = () => {
        $scope.pagedPosts = [];

        $scope.posts.map((post, i) => {
            var page = Math.floor(i / $scope.postsPerPage);
            var current = $scope.posts[i];

            return i % $scope.postsPerPage === 0 ? $scope.pagedPosts[page] = [current] : $scope.pagedPosts[page].push(current);
        });
    };

    $scope.prevPage = () => {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    $scope.nextPage = () => {
        if ($scope.currentPage < $scope.pagedPosts.length - 1) {
            $scope.currentPage++;
        }
    };

    $scope.setPage = (n) => {
        $scope.currentPage = n;
    };

    getBlogDataService
      .getData()
      .then((results) => {
        $scope.posts = results;
        $scope.groupToPages();
      });
  }
}