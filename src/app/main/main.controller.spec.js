describe('controllers', () => {
  let emptyArr = [];
  let scope;

  beforeEach(angular.mock.module('angularDemo'));

  beforeEach(inject(($rootScope, $controller) => {
    scope = $rootScope.$new();
    $controller('MainController', { $scope: scope });
  }));

  it('should have a postsPerPage scope equal to 10y', () => {
    expect(scope.postsPerPage).toEqual(10);
  });

  it('should have a pagedPosts scope equal to empty array', () => {
    expect(scope.pagedPosts).toEqual(emptyArr);
  });

  it('should have a currentPage scope equal to 0', () => {
    expect(scope.currentPage).toEqual(0);
  });
});

describe('getBlogDataService', () => {
  let mockData = { posts: { }}
  let scope;
  let service;
  let http;

  beforeEach(angular.mock.module('angularDemo'));

  beforeEach(inject(($rootScope, $controller, $httpBackend, getBlogDataService) => {
    scope = $rootScope.$new();
    service = getBlogDataService;
    http = $httpBackend;

    $controller('MainController', { $scope: scope });

    spyOn(scope, 'groupToPages');
  }));

  it('should have called the groupToPages function', () => {
    var pass = function() {
      expect(service.getData).toHaveBeenCalled();
      expect(scope.groupToPages).toHaveBeenCalled();
    };

    var fail = function(error) {
      expect(error).toBeUndefined();
    };

    http.expectGET('/').respond(200, mockData);
    spyOn(service, 'getData').and.callThrough();
    service.getData()
      .then(pass)
      .catch(fail);
  });
});