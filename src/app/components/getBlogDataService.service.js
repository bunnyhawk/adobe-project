export class GetBlogDataService {
  constructor ($log, $http) {
    'ngInject';

    this.$log = $log;
    this.$http = $http;
    this.apiHost = 'https://public-api.wordpress.com/rest/v1/sites/idcdistro.wordpress.com/posts/';
  }

  getData() {
    return this.$http.get(this.apiHost)
      .then((response) => {
        return response.data.posts;
      })
      .catch((error) => {
        this.$log.error('XHR Failed for getContributors.\n' + angular.toJson(error.data, true));
      });
  }
}
