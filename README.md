
# Getting Started

#### 1. Install gulp globally:

```sh
$ npm install -g gulp
```

#### 2. Install npm dependencies from the root of your project:

```sh
$ npm install
```


#### 3. Run gulp:

```sh
$ gulp
```

#### 3a. If any errors are thrown, try running bower separately

```sh
$ bower install
$ gulp
```

#### 4. Spin up a server:

```sh
$ gulp build
```

#### 5. Run unit tests:

```sh
$ gulp test
```