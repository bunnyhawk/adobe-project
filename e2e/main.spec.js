'use strict';

describe('The main view', function () {
  var page;
  var deferred;

  beforeEach(function () {
    // TODO: Fix E2E to accomodate promise flow
    deferred = protractor.promise.defer();

    browser.get('/index.html');
    page = require('./main.po');
  });

  it('should include posts with correct data', function() {
    expect(page.post.length).toBe(10);
    expect(page.h1El[0].getText()).toBe('Modern Drummer Interview – “Narada Michael Walden: Drummer, Singer, Songwriter, Educator, and Producer to the Stars”');
  });

});
